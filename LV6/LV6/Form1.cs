﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace LV6
{
    public enum Colors
    {
        black,
        white,
        red,
        green,
        blue,
        cyan,
        magenta,
        yellow,
        orange,
        purple,
        pink
    }

    public partial class Form1 : Form
    {
        private int min_xy_axis = 0;
        private int max_x_axis = 800;
        private int max_y_axis = 600;
        private int max_polygon_points = 10;

        TcpClient client;
        private static readonly Random random = new Random();

        private int generate_random_number(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {
            if (client == null
                && !string.IsNullOrEmpty(tb_host.Text)
                && !string.IsNullOrEmpty(tb_port.Text))
            {
                try
                {
                    client = new TcpClient(tb_host.Text, Int32.Parse(tb_port.Text));
                    panel_connection.BackColor = Color.Green;
                }
                catch(Exception exception) 
                {
                    MessageBox.Show(exception.Message, "Connection Error");
                }
                
            }
        }

        private void btn_close_connection_Click(object sender, EventArgs e)
        {
            if(client != null)
            { 
                client.Close();
                client = null;
                panel_connection.BackColor = Color.Red;
            }
        }

        private void send_data(string message)
        {
            int byteCount = Encoding.ASCII.GetByteCount(message);
            byte[] sendData = new byte[byteCount];
            sendData = Encoding.ASCII.GetBytes(message);

            try
            {
                NetworkStream stream = client.GetStream();
                stream.Write(sendData, 0, sendData.Length);
                stream.Flush();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Send Error");
            }
        }

        private string get_random_color()
        {
            Array colors = Enum.GetValues(typeof(Colors));
            Colors randomColor = (Colors)colors.GetValue(random.Next(colors.Length));
            return randomColor.ToString();
        }

        private void btn_line_send_Click(object sender, EventArgs e)
        {
            if(client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if(client != null 
                && !string.IsNullOrEmpty(tb_line_color.Text)
                && !string.IsNullOrEmpty(tb_line_x1.Text)
                && !string.IsNullOrEmpty(tb_line_y1.Text)
                && !string.IsNullOrEmpty(tb_line_x2.Text)
                && !string.IsNullOrEmpty(tb_line_y2.Text))
            {
                string message = "Line "
                            + tb_line_color.Text + " "
                            + tb_line_x1.Text + " "
                            + tb_line_y1.Text + " "
                            + tb_line_x2.Text + " "
                            + tb_line_y2.Text;

                send_data(message);

                tb_line_color.Clear();
                tb_line_x1.Clear();
                tb_line_y1.Clear();
                tb_line_x2.Clear();
                tb_line_y2.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_line_random_Click(object sender, EventArgs e)
        {
            tb_line_color.Text = get_random_color();
            tb_line_x1.Text = generate_random_number(min_xy_axis, max_x_axis).ToString();
            tb_line_y1.Text = generate_random_number(min_xy_axis, max_y_axis).ToString();
            tb_line_x2.Text = generate_random_number(min_xy_axis, max_x_axis).ToString();
            tb_line_y2.Text = generate_random_number(min_xy_axis, max_y_axis).ToString();
        }

        private static bool is_triangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            return ((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3)) != 0;
        }

        private void btn_triangle_send_Click(object sender, EventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if (client != null
                && !string.IsNullOrEmpty(tb_triangle_color.Text)
                && !string.IsNullOrEmpty(tb_triangle_x1.Text)
                && !string.IsNullOrEmpty(tb_triangle_y1.Text)
                && !string.IsNullOrEmpty(tb_triangle_x2.Text)
                && !string.IsNullOrEmpty(tb_triangle_y2.Text)
                && !string.IsNullOrEmpty(tb_triangle_x3.Text)
                && !string.IsNullOrEmpty(tb_triangle_y3.Text))
            {
                if(is_triangle(Int32.Parse(tb_triangle_x1.Text),
                                Int32.Parse(tb_triangle_y1.Text),
                                Int32.Parse(tb_triangle_x2.Text),
                                Int32.Parse(tb_triangle_y2.Text),
                                Int32.Parse(tb_triangle_x3.Text),
                                Int32.Parse(tb_triangle_y3.Text)))
                {
                    string message = "Triangle "
                            + tb_triangle_color.Text + " "
                            + tb_triangle_x1.Text + " "
                            + tb_triangle_y1.Text + " "
                            + tb_triangle_x2.Text + " "
                            + tb_triangle_y2.Text + " "
                            + tb_triangle_x3.Text + " "
                            + tb_triangle_y3.Text;

                    send_data(message);

                    tb_triangle_color.Clear();
                    tb_triangle_x1.Clear();
                    tb_triangle_y1.Clear();
                    tb_triangle_x2.Clear();
                    tb_triangle_y2.Clear();
                    tb_triangle_x3.Clear();
                    tb_triangle_y3.Clear();
                }
                else
                {
                    MessageBox.Show("Entered values don't form triangle!", "Error");
                }
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_triangle_random_Click(object sender, EventArgs e)
        {
            int x1 = generate_random_number(min_xy_axis, max_x_axis);
            int y1 = generate_random_number(min_xy_axis, max_y_axis);
            int x2 = generate_random_number(min_xy_axis, max_x_axis);
            int y2 = generate_random_number(min_xy_axis, max_y_axis);
            int x3 = generate_random_number(min_xy_axis, max_x_axis);
            int y3 = generate_random_number(min_xy_axis, max_y_axis);

            tb_triangle_color.Text = get_random_color();

            while (!is_triangle(x1, y1, x2, y2, x3, y3))
            {
                x1 = generate_random_number(min_xy_axis, max_x_axis);
                y1 = generate_random_number(min_xy_axis, max_y_axis);
                x2 = generate_random_number(min_xy_axis, max_x_axis);
                y2 = generate_random_number(min_xy_axis, max_y_axis);
                x3 = generate_random_number(min_xy_axis, max_x_axis);
                y3 = generate_random_number(min_xy_axis, max_y_axis);
            }

            tb_triangle_x1.Text = x1.ToString();
            tb_triangle_y1.Text = y1.ToString();
            tb_triangle_x2.Text = x2.ToString();
            tb_triangle_y2.Text = y2.ToString();
            tb_triangle_x3.Text = x3.ToString();
            tb_triangle_y3.Text = y3.ToString();
        }

        private void btn_rectangle_send_Click(object sender, EventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if (client != null
                && !string.IsNullOrEmpty(tb_rectangle_color.Text)
                && !string.IsNullOrEmpty(tb_rectangle_x1.Text)
                && !string.IsNullOrEmpty(tb_rectangle_y1.Text)
                && !string.IsNullOrEmpty(tb_rectangle_height.Text)
                && !string.IsNullOrEmpty(tb_rectangle_width.Text))
            {
                string message = "Rectangle "
                        + tb_rectangle_color.Text + " "
                        + tb_rectangle_x1.Text + " "
                        + tb_rectangle_y1.Text + " "
                        + tb_rectangle_height.Text + " "
                        + tb_rectangle_width.Text;

                send_data(message);

                tb_rectangle_color.Clear();
                tb_rectangle_x1.Clear();
                tb_rectangle_y1.Clear();
                tb_rectangle_height.Clear();
                tb_rectangle_width.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_rectangle_random_Click(object sender, EventArgs e)
        {
            int x1 = generate_random_number(min_xy_axis, max_x_axis - 1);
            int y1 = generate_random_number(min_xy_axis, max_y_axis - 1);

            tb_rectangle_color.Text = get_random_color();
            tb_rectangle_x1.Text = x1.ToString();
            tb_rectangle_y1.Text = y1.ToString();
            tb_rectangle_width.Text = generate_random_number(1, max_x_axis - x1).ToString();
            tb_rectangle_height.Text = generate_random_number(1, max_y_axis - y1).ToString();            
        }

        private List<Point> get_points_from_listbox()
        {
            List<Point> points = new List<Point>();

            foreach(var item in list_polygon.Items)
            {
                string point = item.ToString();
                string[] coordinates = point.Split(' ');
                int x, y;

                if (int.TryParse(coordinates[0], out x) && int.TryParse(coordinates[1], out y))
                {
                    points.Add(new Point(x, y));
                }
            }
            return points;
        }

        private void btn_polygon_send_Click(object sender, EventArgs e)
        {
            List<Point> points = get_points_from_listbox();
            int count = points.Count;

            if (client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if (client != null
                && !string.IsNullOrEmpty(tb_polygon_color.Text)
                && list_polygon.Items.Count >= 3) 
            {
                StringBuilder messageBuilder = new StringBuilder();

                messageBuilder.Append($"Polygon {tb_polygon_color.Text} ");

                foreach (Point point in points)
                {
                    messageBuilder.Append($"{point.X} {point.Y} ");
                }

                send_data(messageBuilder.ToString());

                list_polygon.Items.Clear();
                tb_polygon_color.Clear();
                tb_polygon_x1.Clear();
                tb_polygon_y1.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_polygon_add_point_Click(object sender, EventArgs e)
        {
            string point; 

            if(!string.IsNullOrEmpty(tb_polygon_x1.Text) 
                && !string.IsNullOrEmpty(tb_polygon_y1.Text))
            {
                point = tb_polygon_x1.Text + " " + tb_polygon_y1.Text;
                list_polygon.Items.Add(point);

                tb_polygon_x1.Clear();
                tb_polygon_y1.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }            
        }

        private void btn_polygon_random_Click(object sender, EventArgs e)
        {
            list_polygon.Items.Clear();

            int count = generate_random_number(3, max_polygon_points);
   
            List<Point> points = new List<Point>();
            for (int i = 0; i < count; i++)
            {
                points.Add(new Point(generate_random_number(1, max_x_axis), generate_random_number(1, max_y_axis)));
            }

            Point lowestPoint = points.OrderBy(p => p.Y).ThenBy(p => p.X).First();
            points = points.OrderBy(p => Math.Atan2(p.Y - lowestPoint.Y, p.X - lowestPoint.X)).ToList();

            tb_polygon_color.Text = get_random_color();

            foreach (var item in points)
            {
                string point = $"{item.X} {item.Y}";
                list_polygon.Items.Add(point);
            }
        }

        private void btn_circle_send_Click(object sender, EventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if (client != null
                && !string.IsNullOrEmpty(tb_circle_color.Text)
                && !string.IsNullOrEmpty(tb_circle_x1.Text)
                && !string.IsNullOrEmpty(tb_circle_y1.Text)
                && !string.IsNullOrEmpty(tb_circle_radius.Text))
            {
                string message = "Circle "
                        + tb_circle_color.Text + " "
                        + tb_circle_x1.Text + " "
                        + tb_circle_y1.Text + " "
                        + tb_circle_radius.Text;

                send_data(message);

                tb_circle_color.Clear();
                tb_circle_x1.Clear();
                tb_circle_y1.Clear();
                tb_circle_radius.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_circle_random_Click(object sender, EventArgs e)
        {
            int x1 = generate_random_number(min_xy_axis, max_x_axis - 1);
            int y1 = generate_random_number(min_xy_axis, max_y_axis - 1);
            int[] radius = {(x1 - 1), (y1 - 1), (max_x_axis - x1), (max_y_axis - y1)};
            int max_radius = radius.Min();

            tb_circle_color.Text = get_random_color();
            tb_circle_x1.Text = x1.ToString();
            tb_circle_y1.Text = y1.ToString();
            tb_circle_radius.Text = generate_random_number(1, max_radius).ToString();
        }

        private void btn_ellipse_send_Click(object sender, EventArgs e)
        {
            if (client == null)
            {
                MessageBox.Show("Establish connection first!", "Connection Error");
            }
            else if (client != null
                && !string.IsNullOrEmpty(tb_ellipse_color.Text)
                && !string.IsNullOrEmpty(tb_ellipse_x1.Text)
                && !string.IsNullOrEmpty(tb_ellipse_y1.Text)
                && !string.IsNullOrEmpty(tb_ellipse_rx.Text)
                && !string.IsNullOrEmpty(tb_ellipse_ry.Text))
            {
                string message = "Ellipse "
                        + tb_ellipse_color.Text + " "
                        + tb_ellipse_x1.Text + " "
                        + tb_ellipse_y1.Text + " "
                        + tb_ellipse_rx.Text + " "
                        + tb_ellipse_ry.Text;

                send_data(message);

                tb_ellipse_color.Clear();
                tb_ellipse_x1.Clear();
                tb_ellipse_y1.Clear();
                tb_ellipse_rx.Clear();
                tb_ellipse_ry.Clear();
            }
            else
            {
                MessageBox.Show("Fill all input boxes!", "Error");
            }
        }

        private void btn_ellipse_random_Click(object sender, EventArgs e)
        {
            int x1 = generate_random_number(min_xy_axis, max_x_axis - 1);
            int y1 = generate_random_number(min_xy_axis, max_y_axis - 1);

            int max_x_radius = Math.Min((x1 - 1), (max_x_axis - x1));
            int max_y_radius = Math.Min((y1 - 1), (max_y_axis - y1));

            tb_ellipse_color.Text = get_random_color();
            tb_ellipse_x1.Text = x1.ToString();
            tb_ellipse_y1.Text = y1.ToString();
            tb_ellipse_rx.Text = generate_random_number(1, max_x_radius).ToString();
            tb_ellipse_ry.Text = generate_random_number(1, max_y_radius).ToString();
        }
    }
}
