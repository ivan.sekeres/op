﻿namespace LV6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tab_connect = new System.Windows.Forms.TabPage();
            this.btn_close_connection = new System.Windows.Forms.Button();
            this.btn_connect = new System.Windows.Forms.Button();
            this.tb_port = new System.Windows.Forms.TextBox();
            this.tb_host = new System.Windows.Forms.TextBox();
            this.lb_port = new System.Windows.Forms.Label();
            this.lb_host = new System.Windows.Forms.Label();
            this.tab_line = new System.Windows.Forms.TabPage();
            this.lb_line_color = new System.Windows.Forms.Label();
            this.lb_line_x2 = new System.Windows.Forms.Label();
            this.lb_line_y2 = new System.Windows.Forms.Label();
            this.lb_line_y1 = new System.Windows.Forms.Label();
            this.lb_line_x1 = new System.Windows.Forms.Label();
            this.tb_line_color = new System.Windows.Forms.TextBox();
            this.tb_line_y2 = new System.Windows.Forms.TextBox();
            this.tb_line_x2 = new System.Windows.Forms.TextBox();
            this.tb_line_y1 = new System.Windows.Forms.TextBox();
            this.tb_line_x1 = new System.Windows.Forms.TextBox();
            this.btn_line_random = new System.Windows.Forms.Button();
            this.btn_line_send = new System.Windows.Forms.Button();
            this.tab_triangle = new System.Windows.Forms.TabPage();
            this.lb_trangle_x3 = new System.Windows.Forms.Label();
            this.lb_trangle_y3 = new System.Windows.Forms.Label();
            this.tb_triangle_y3 = new System.Windows.Forms.TextBox();
            this.tb_triangle_x3 = new System.Windows.Forms.TextBox();
            this.lb_triangle_color = new System.Windows.Forms.Label();
            this.lb_trangle_x2 = new System.Windows.Forms.Label();
            this.lb_trangle_y2 = new System.Windows.Forms.Label();
            this.lb_triangle_y1 = new System.Windows.Forms.Label();
            this.lb_triangle_x1 = new System.Windows.Forms.Label();
            this.tb_triangle_color = new System.Windows.Forms.TextBox();
            this.tb_triangle_y2 = new System.Windows.Forms.TextBox();
            this.tb_triangle_x2 = new System.Windows.Forms.TextBox();
            this.tb_triangle_y1 = new System.Windows.Forms.TextBox();
            this.tb_triangle_x1 = new System.Windows.Forms.TextBox();
            this.btn_triangle_random = new System.Windows.Forms.Button();
            this.btn_triangle_send = new System.Windows.Forms.Button();
            this.tab_rectangle = new System.Windows.Forms.TabPage();
            this.lb_rectangle_color = new System.Windows.Forms.Label();
            this.lb_rectangle_height = new System.Windows.Forms.Label();
            this.lb_rectangle_width = new System.Windows.Forms.Label();
            this.lb_rectangle_y1 = new System.Windows.Forms.Label();
            this.lb_rectangle_x1 = new System.Windows.Forms.Label();
            this.tb_rectangle_color = new System.Windows.Forms.TextBox();
            this.tb_rectangle_width = new System.Windows.Forms.TextBox();
            this.tb_rectangle_height = new System.Windows.Forms.TextBox();
            this.tb_rectangle_y1 = new System.Windows.Forms.TextBox();
            this.tb_rectangle_x1 = new System.Windows.Forms.TextBox();
            this.btn_rectangle_random = new System.Windows.Forms.Button();
            this.btn_rectangle_send = new System.Windows.Forms.Button();
            this.tab_polygon = new System.Windows.Forms.TabPage();
            this.btn_polygon_add_point = new System.Windows.Forms.Button();
            this.lb_polygon_color = new System.Windows.Forms.Label();
            this.lb_polygon_y1 = new System.Windows.Forms.Label();
            this.lb_polygon_x1 = new System.Windows.Forms.Label();
            this.tb_polygon_color = new System.Windows.Forms.TextBox();
            this.tb_polygon_y1 = new System.Windows.Forms.TextBox();
            this.tb_polygon_x1 = new System.Windows.Forms.TextBox();
            this.btn_polygon_random = new System.Windows.Forms.Button();
            this.btn_polygon_send = new System.Windows.Forms.Button();
            this.list_polygon = new System.Windows.Forms.ListBox();
            this.tab_circle = new System.Windows.Forms.TabPage();
            this.lb_circle_color = new System.Windows.Forms.Label();
            this.lb_circle_radius = new System.Windows.Forms.Label();
            this.lb_circle_y1 = new System.Windows.Forms.Label();
            this.lb_circle_x1 = new System.Windows.Forms.Label();
            this.tb_circle_color = new System.Windows.Forms.TextBox();
            this.tb_circle_radius = new System.Windows.Forms.TextBox();
            this.tb_circle_y1 = new System.Windows.Forms.TextBox();
            this.tb_circle_x1 = new System.Windows.Forms.TextBox();
            this.btn_circle_random = new System.Windows.Forms.Button();
            this.btn_circle_send = new System.Windows.Forms.Button();
            this.tab_ellipse = new System.Windows.Forms.TabPage();
            this.lb_ellipse_ry = new System.Windows.Forms.Label();
            this.tb_ellipse_ry = new System.Windows.Forms.TextBox();
            this.lb_ellipse_color = new System.Windows.Forms.Label();
            this.lb_ellipse_rx = new System.Windows.Forms.Label();
            this.lb_ellipse_y1 = new System.Windows.Forms.Label();
            this.lb_ellipse_x1 = new System.Windows.Forms.Label();
            this.tb_ellipse_color = new System.Windows.Forms.TextBox();
            this.tb_ellipse_rx = new System.Windows.Forms.TextBox();
            this.tb_ellipse_y1 = new System.Windows.Forms.TextBox();
            this.tb_ellipse_x1 = new System.Windows.Forms.TextBox();
            this.btn_ellipse_random = new System.Windows.Forms.Button();
            this.btn_ellipse_send = new System.Windows.Forms.Button();
            this.lb_connection = new System.Windows.Forms.Label();
            this.panel_connection = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabs.SuspendLayout();
            this.tab_connect.SuspendLayout();
            this.tab_line.SuspendLayout();
            this.tab_triangle.SuspendLayout();
            this.tab_rectangle.SuspendLayout();
            this.tab_polygon.SuspendLayout();
            this.tab_circle.SuspendLayout();
            this.tab_ellipse.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.AccessibleDescription = "";
            this.tabs.AccessibleName = "";
            this.tabs.Controls.Add(this.tab_connect);
            this.tabs.Controls.Add(this.tab_line);
            this.tabs.Controls.Add(this.tab_triangle);
            this.tabs.Controls.Add(this.tab_rectangle);
            this.tabs.Controls.Add(this.tab_polygon);
            this.tabs.Controls.Add(this.tab_circle);
            this.tabs.Controls.Add(this.tab_ellipse);
            this.tabs.Location = new System.Drawing.Point(12, 12);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(804, 537);
            this.tabs.TabIndex = 0;
            // 
            // tab_connect
            // 
            this.tab_connect.Controls.Add(this.btn_close_connection);
            this.tab_connect.Controls.Add(this.btn_connect);
            this.tab_connect.Controls.Add(this.tb_port);
            this.tab_connect.Controls.Add(this.tb_host);
            this.tab_connect.Controls.Add(this.lb_port);
            this.tab_connect.Controls.Add(this.lb_host);
            this.tab_connect.Location = new System.Drawing.Point(4, 22);
            this.tab_connect.Name = "tab_connect";
            this.tab_connect.Padding = new System.Windows.Forms.Padding(3);
            this.tab_connect.Size = new System.Drawing.Size(796, 511);
            this.tab_connect.TabIndex = 0;
            this.tab_connect.Text = "Connect";
            this.tab_connect.UseVisualStyleBackColor = true;
            // 
            // btn_close_connection
            // 
            this.btn_close_connection.Location = new System.Drawing.Point(466, 327);
            this.btn_close_connection.Name = "btn_close_connection";
            this.btn_close_connection.Size = new System.Drawing.Size(180, 100);
            this.btn_close_connection.TabIndex = 5;
            this.btn_close_connection.Text = "Close connection";
            this.btn_close_connection.UseVisualStyleBackColor = true;
            this.btn_close_connection.Click += new System.EventHandler(this.btn_close_connection_Click);
            // 
            // btn_connect
            // 
            this.btn_connect.Location = new System.Drawing.Point(91, 327);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(180, 100);
            this.btn_connect.TabIndex = 4;
            this.btn_connect.Text = "Connect";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // tb_port
            // 
            this.tb_port.Location = new System.Drawing.Point(96, 78);
            this.tb_port.Name = "tb_port";
            this.tb_port.Size = new System.Drawing.Size(150, 20);
            this.tb_port.TabIndex = 3;
            this.tb_port.Text = "8000";
            // 
            // tb_host
            // 
            this.tb_host.Location = new System.Drawing.Point(96, 34);
            this.tb_host.Name = "tb_host";
            this.tb_host.Size = new System.Drawing.Size(150, 20);
            this.tb_host.TabIndex = 2;
            this.tb_host.Text = "localhost";
            // 
            // lb_port
            // 
            this.lb_port.AutoSize = true;
            this.lb_port.Location = new System.Drawing.Point(26, 81);
            this.lb_port.Name = "lb_port";
            this.lb_port.Size = new System.Drawing.Size(56, 13);
            this.lb_port.TabIndex = 1;
            this.lb_port.Text = "Host port: ";
            // 
            // lb_host
            // 
            this.lb_host.AutoSize = true;
            this.lb_host.Location = new System.Drawing.Point(26, 37);
            this.lb_host.Name = "lb_host";
            this.lb_host.Size = new System.Drawing.Size(64, 13);
            this.lb_host.TabIndex = 0;
            this.lb_host.Text = "Host name: ";
            // 
            // tab_line
            // 
            this.tab_line.Controls.Add(this.lb_line_color);
            this.tab_line.Controls.Add(this.lb_line_x2);
            this.tab_line.Controls.Add(this.lb_line_y2);
            this.tab_line.Controls.Add(this.lb_line_y1);
            this.tab_line.Controls.Add(this.lb_line_x1);
            this.tab_line.Controls.Add(this.tb_line_color);
            this.tab_line.Controls.Add(this.tb_line_y2);
            this.tab_line.Controls.Add(this.tb_line_x2);
            this.tab_line.Controls.Add(this.tb_line_y1);
            this.tab_line.Controls.Add(this.tb_line_x1);
            this.tab_line.Controls.Add(this.btn_line_random);
            this.tab_line.Controls.Add(this.btn_line_send);
            this.tab_line.Location = new System.Drawing.Point(4, 22);
            this.tab_line.Name = "tab_line";
            this.tab_line.Padding = new System.Windows.Forms.Padding(3);
            this.tab_line.Size = new System.Drawing.Size(796, 511);
            this.tab_line.TabIndex = 1;
            this.tab_line.Text = "Line";
            this.tab_line.UseVisualStyleBackColor = true;
            // 
            // lb_line_color
            // 
            this.lb_line_color.AutoSize = true;
            this.lb_line_color.Location = new System.Drawing.Point(400, 30);
            this.lb_line_color.Name = "lb_line_color";
            this.lb_line_color.Size = new System.Drawing.Size(37, 13);
            this.lb_line_color.TabIndex = 11;
            this.lb_line_color.Text = "Color: ";
            // 
            // lb_line_x2
            // 
            this.lb_line_x2.AutoSize = true;
            this.lb_line_x2.Location = new System.Drawing.Point(20, 90);
            this.lb_line_x2.Name = "lb_line_x2";
            this.lb_line_x2.Size = new System.Drawing.Size(48, 13);
            this.lb_line_x2.TabIndex = 10;
            this.lb_line_x2.Text = "Point x2:";
            // 
            // lb_line_y2
            // 
            this.lb_line_y2.AutoSize = true;
            this.lb_line_y2.Location = new System.Drawing.Point(20, 120);
            this.lb_line_y2.Name = "lb_line_y2";
            this.lb_line_y2.Size = new System.Drawing.Size(48, 13);
            this.lb_line_y2.TabIndex = 9;
            this.lb_line_y2.Text = "Point y2:";
            // 
            // lb_line_y1
            // 
            this.lb_line_y1.AutoSize = true;
            this.lb_line_y1.Location = new System.Drawing.Point(20, 60);
            this.lb_line_y1.Name = "lb_line_y1";
            this.lb_line_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_line_y1.TabIndex = 8;
            this.lb_line_y1.Text = "Point y1:";
            // 
            // lb_line_x1
            // 
            this.lb_line_x1.AutoSize = true;
            this.lb_line_x1.Location = new System.Drawing.Point(20, 30);
            this.lb_line_x1.Name = "lb_line_x1";
            this.lb_line_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_line_x1.TabIndex = 7;
            this.lb_line_x1.Text = "Point x1: ";
            // 
            // tb_line_color
            // 
            this.tb_line_color.Location = new System.Drawing.Point(440, 25);
            this.tb_line_color.Name = "tb_line_color";
            this.tb_line_color.Size = new System.Drawing.Size(150, 20);
            this.tb_line_color.TabIndex = 6;
            // 
            // tb_line_y2
            // 
            this.tb_line_y2.Location = new System.Drawing.Point(75, 115);
            this.tb_line_y2.Name = "tb_line_y2";
            this.tb_line_y2.Size = new System.Drawing.Size(190, 20);
            this.tb_line_y2.TabIndex = 5;
            // 
            // tb_line_x2
            // 
            this.tb_line_x2.Location = new System.Drawing.Point(75, 85);
            this.tb_line_x2.Name = "tb_line_x2";
            this.tb_line_x2.Size = new System.Drawing.Size(190, 20);
            this.tb_line_x2.TabIndex = 4;
            // 
            // tb_line_y1
            // 
            this.tb_line_y1.Location = new System.Drawing.Point(75, 55);
            this.tb_line_y1.Name = "tb_line_y1";
            this.tb_line_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_line_y1.TabIndex = 3;
            // 
            // tb_line_x1
            // 
            this.tb_line_x1.Location = new System.Drawing.Point(75, 25);
            this.tb_line_x1.Name = "tb_line_x1";
            this.tb_line_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_line_x1.TabIndex = 2;
            // 
            // btn_line_random
            // 
            this.btn_line_random.Location = new System.Drawing.Point(440, 90);
            this.btn_line_random.Name = "btn_line_random";
            this.btn_line_random.Size = new System.Drawing.Size(150, 50);
            this.btn_line_random.TabIndex = 1;
            this.btn_line_random.Text = "Random";
            this.btn_line_random.UseVisualStyleBackColor = true;
            this.btn_line_random.Click += new System.EventHandler(this.btn_line_random_Click);
            // 
            // btn_line_send
            // 
            this.btn_line_send.Location = new System.Drawing.Point(300, 330);
            this.btn_line_send.Name = "btn_line_send";
            this.btn_line_send.Size = new System.Drawing.Size(180, 100);
            this.btn_line_send.TabIndex = 0;
            this.btn_line_send.Text = "Send";
            this.btn_line_send.UseVisualStyleBackColor = true;
            this.btn_line_send.Click += new System.EventHandler(this.btn_line_send_Click);
            // 
            // tab_triangle
            // 
            this.tab_triangle.Controls.Add(this.lb_trangle_x3);
            this.tab_triangle.Controls.Add(this.lb_trangle_y3);
            this.tab_triangle.Controls.Add(this.tb_triangle_y3);
            this.tab_triangle.Controls.Add(this.tb_triangle_x3);
            this.tab_triangle.Controls.Add(this.lb_triangle_color);
            this.tab_triangle.Controls.Add(this.lb_trangle_x2);
            this.tab_triangle.Controls.Add(this.lb_trangle_y2);
            this.tab_triangle.Controls.Add(this.lb_triangle_y1);
            this.tab_triangle.Controls.Add(this.lb_triangle_x1);
            this.tab_triangle.Controls.Add(this.tb_triangle_color);
            this.tab_triangle.Controls.Add(this.tb_triangle_y2);
            this.tab_triangle.Controls.Add(this.tb_triangle_x2);
            this.tab_triangle.Controls.Add(this.tb_triangle_y1);
            this.tab_triangle.Controls.Add(this.tb_triangle_x1);
            this.tab_triangle.Controls.Add(this.btn_triangle_random);
            this.tab_triangle.Controls.Add(this.btn_triangle_send);
            this.tab_triangle.Location = new System.Drawing.Point(4, 22);
            this.tab_triangle.Name = "tab_triangle";
            this.tab_triangle.Padding = new System.Windows.Forms.Padding(3);
            this.tab_triangle.Size = new System.Drawing.Size(796, 511);
            this.tab_triangle.TabIndex = 2;
            this.tab_triangle.Text = "Triangle";
            this.tab_triangle.UseVisualStyleBackColor = true;
            // 
            // lb_trangle_x3
            // 
            this.lb_trangle_x3.AutoSize = true;
            this.lb_trangle_x3.Location = new System.Drawing.Point(20, 150);
            this.lb_trangle_x3.Name = "lb_trangle_x3";
            this.lb_trangle_x3.Size = new System.Drawing.Size(48, 13);
            this.lb_trangle_x3.TabIndex = 27;
            this.lb_trangle_x3.Text = "Point x3:";
            // 
            // lb_trangle_y3
            // 
            this.lb_trangle_y3.AutoSize = true;
            this.lb_trangle_y3.Location = new System.Drawing.Point(20, 180);
            this.lb_trangle_y3.Name = "lb_trangle_y3";
            this.lb_trangle_y3.Size = new System.Drawing.Size(48, 13);
            this.lb_trangle_y3.TabIndex = 26;
            this.lb_trangle_y3.Text = "Point y3:";
            // 
            // tb_triangle_y3
            // 
            this.tb_triangle_y3.Location = new System.Drawing.Point(75, 175);
            this.tb_triangle_y3.Name = "tb_triangle_y3";
            this.tb_triangle_y3.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_y3.TabIndex = 25;
            // 
            // tb_triangle_x3
            // 
            this.tb_triangle_x3.Location = new System.Drawing.Point(75, 145);
            this.tb_triangle_x3.Name = "tb_triangle_x3";
            this.tb_triangle_x3.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_x3.TabIndex = 24;
            // 
            // lb_triangle_color
            // 
            this.lb_triangle_color.AutoSize = true;
            this.lb_triangle_color.Location = new System.Drawing.Point(400, 30);
            this.lb_triangle_color.Name = "lb_triangle_color";
            this.lb_triangle_color.Size = new System.Drawing.Size(37, 13);
            this.lb_triangle_color.TabIndex = 23;
            this.lb_triangle_color.Text = "Color: ";
            // 
            // lb_trangle_x2
            // 
            this.lb_trangle_x2.AutoSize = true;
            this.lb_trangle_x2.Location = new System.Drawing.Point(20, 90);
            this.lb_trangle_x2.Name = "lb_trangle_x2";
            this.lb_trangle_x2.Size = new System.Drawing.Size(48, 13);
            this.lb_trangle_x2.TabIndex = 22;
            this.lb_trangle_x2.Text = "Point x2:";
            // 
            // lb_trangle_y2
            // 
            this.lb_trangle_y2.AutoSize = true;
            this.lb_trangle_y2.Location = new System.Drawing.Point(20, 120);
            this.lb_trangle_y2.Name = "lb_trangle_y2";
            this.lb_trangle_y2.Size = new System.Drawing.Size(48, 13);
            this.lb_trangle_y2.TabIndex = 21;
            this.lb_trangle_y2.Text = "Point y2:";
            // 
            // lb_triangle_y1
            // 
            this.lb_triangle_y1.AutoSize = true;
            this.lb_triangle_y1.Location = new System.Drawing.Point(20, 60);
            this.lb_triangle_y1.Name = "lb_triangle_y1";
            this.lb_triangle_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_triangle_y1.TabIndex = 20;
            this.lb_triangle_y1.Text = "Point y1:";
            // 
            // lb_triangle_x1
            // 
            this.lb_triangle_x1.AutoSize = true;
            this.lb_triangle_x1.Location = new System.Drawing.Point(20, 30);
            this.lb_triangle_x1.Name = "lb_triangle_x1";
            this.lb_triangle_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_triangle_x1.TabIndex = 19;
            this.lb_triangle_x1.Text = "Point x1: ";
            // 
            // tb_triangle_color
            // 
            this.tb_triangle_color.Location = new System.Drawing.Point(440, 25);
            this.tb_triangle_color.Name = "tb_triangle_color";
            this.tb_triangle_color.Size = new System.Drawing.Size(150, 20);
            this.tb_triangle_color.TabIndex = 18;
            // 
            // tb_triangle_y2
            // 
            this.tb_triangle_y2.Location = new System.Drawing.Point(75, 115);
            this.tb_triangle_y2.Name = "tb_triangle_y2";
            this.tb_triangle_y2.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_y2.TabIndex = 17;
            // 
            // tb_triangle_x2
            // 
            this.tb_triangle_x2.Location = new System.Drawing.Point(75, 85);
            this.tb_triangle_x2.Name = "tb_triangle_x2";
            this.tb_triangle_x2.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_x2.TabIndex = 16;
            // 
            // tb_triangle_y1
            // 
            this.tb_triangle_y1.Location = new System.Drawing.Point(75, 55);
            this.tb_triangle_y1.Name = "tb_triangle_y1";
            this.tb_triangle_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_y1.TabIndex = 15;
            // 
            // tb_triangle_x1
            // 
            this.tb_triangle_x1.Location = new System.Drawing.Point(75, 25);
            this.tb_triangle_x1.Name = "tb_triangle_x1";
            this.tb_triangle_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_triangle_x1.TabIndex = 14;
            // 
            // btn_triangle_random
            // 
            this.btn_triangle_random.Location = new System.Drawing.Point(440, 90);
            this.btn_triangle_random.Name = "btn_triangle_random";
            this.btn_triangle_random.Size = new System.Drawing.Size(150, 50);
            this.btn_triangle_random.TabIndex = 13;
            this.btn_triangle_random.Text = "Random";
            this.btn_triangle_random.UseVisualStyleBackColor = true;
            this.btn_triangle_random.Click += new System.EventHandler(this.btn_triangle_random_Click);
            // 
            // btn_triangle_send
            // 
            this.btn_triangle_send.Location = new System.Drawing.Point(300, 330);
            this.btn_triangle_send.Name = "btn_triangle_send";
            this.btn_triangle_send.Size = new System.Drawing.Size(180, 100);
            this.btn_triangle_send.TabIndex = 12;
            this.btn_triangle_send.Text = "Send";
            this.btn_triangle_send.UseVisualStyleBackColor = true;
            this.btn_triangle_send.Click += new System.EventHandler(this.btn_triangle_send_Click);
            // 
            // tab_rectangle
            // 
            this.tab_rectangle.AccessibleName = "";
            this.tab_rectangle.Controls.Add(this.lb_rectangle_color);
            this.tab_rectangle.Controls.Add(this.lb_rectangle_height);
            this.tab_rectangle.Controls.Add(this.lb_rectangle_width);
            this.tab_rectangle.Controls.Add(this.lb_rectangle_y1);
            this.tab_rectangle.Controls.Add(this.lb_rectangle_x1);
            this.tab_rectangle.Controls.Add(this.tb_rectangle_color);
            this.tab_rectangle.Controls.Add(this.tb_rectangle_width);
            this.tab_rectangle.Controls.Add(this.tb_rectangle_height);
            this.tab_rectangle.Controls.Add(this.tb_rectangle_y1);
            this.tab_rectangle.Controls.Add(this.tb_rectangle_x1);
            this.tab_rectangle.Controls.Add(this.btn_rectangle_random);
            this.tab_rectangle.Controls.Add(this.btn_rectangle_send);
            this.tab_rectangle.Location = new System.Drawing.Point(4, 22);
            this.tab_rectangle.Name = "tab_rectangle";
            this.tab_rectangle.Padding = new System.Windows.Forms.Padding(3);
            this.tab_rectangle.Size = new System.Drawing.Size(796, 511);
            this.tab_rectangle.TabIndex = 3;
            this.tab_rectangle.Text = "Rectangle";
            this.tab_rectangle.UseVisualStyleBackColor = true;
            // 
            // lb_rectangle_color
            // 
            this.lb_rectangle_color.AutoSize = true;
            this.lb_rectangle_color.Location = new System.Drawing.Point(400, 30);
            this.lb_rectangle_color.Name = "lb_rectangle_color";
            this.lb_rectangle_color.Size = new System.Drawing.Size(37, 13);
            this.lb_rectangle_color.TabIndex = 39;
            this.lb_rectangle_color.Text = "Color: ";
            // 
            // lb_rectangle_height
            // 
            this.lb_rectangle_height.AutoSize = true;
            this.lb_rectangle_height.Location = new System.Drawing.Point(20, 90);
            this.lb_rectangle_height.Name = "lb_rectangle_height";
            this.lb_rectangle_height.Size = new System.Drawing.Size(41, 13);
            this.lb_rectangle_height.TabIndex = 38;
            this.lb_rectangle_height.Text = "Height:";
            // 
            // lb_rectangle_width
            // 
            this.lb_rectangle_width.AutoSize = true;
            this.lb_rectangle_width.Location = new System.Drawing.Point(20, 120);
            this.lb_rectangle_width.Name = "lb_rectangle_width";
            this.lb_rectangle_width.Size = new System.Drawing.Size(38, 13);
            this.lb_rectangle_width.TabIndex = 37;
            this.lb_rectangle_width.Text = "Width:";
            // 
            // lb_rectangle_y1
            // 
            this.lb_rectangle_y1.AutoSize = true;
            this.lb_rectangle_y1.Location = new System.Drawing.Point(20, 60);
            this.lb_rectangle_y1.Name = "lb_rectangle_y1";
            this.lb_rectangle_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_rectangle_y1.TabIndex = 36;
            this.lb_rectangle_y1.Text = "Point y1:";
            // 
            // lb_rectangle_x1
            // 
            this.lb_rectangle_x1.AutoSize = true;
            this.lb_rectangle_x1.Location = new System.Drawing.Point(20, 30);
            this.lb_rectangle_x1.Name = "lb_rectangle_x1";
            this.lb_rectangle_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_rectangle_x1.TabIndex = 35;
            this.lb_rectangle_x1.Text = "Point x1: ";
            // 
            // tb_rectangle_color
            // 
            this.tb_rectangle_color.Location = new System.Drawing.Point(440, 25);
            this.tb_rectangle_color.Name = "tb_rectangle_color";
            this.tb_rectangle_color.Size = new System.Drawing.Size(150, 20);
            this.tb_rectangle_color.TabIndex = 34;
            // 
            // tb_rectangle_width
            // 
            this.tb_rectangle_width.Location = new System.Drawing.Point(75, 115);
            this.tb_rectangle_width.Name = "tb_rectangle_width";
            this.tb_rectangle_width.Size = new System.Drawing.Size(190, 20);
            this.tb_rectangle_width.TabIndex = 33;
            // 
            // tb_rectangle_height
            // 
            this.tb_rectangle_height.Location = new System.Drawing.Point(75, 85);
            this.tb_rectangle_height.Name = "tb_rectangle_height";
            this.tb_rectangle_height.Size = new System.Drawing.Size(190, 20);
            this.tb_rectangle_height.TabIndex = 32;
            // 
            // tb_rectangle_y1
            // 
            this.tb_rectangle_y1.Location = new System.Drawing.Point(75, 55);
            this.tb_rectangle_y1.Name = "tb_rectangle_y1";
            this.tb_rectangle_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_rectangle_y1.TabIndex = 31;
            // 
            // tb_rectangle_x1
            // 
            this.tb_rectangle_x1.Location = new System.Drawing.Point(75, 25);
            this.tb_rectangle_x1.Name = "tb_rectangle_x1";
            this.tb_rectangle_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_rectangle_x1.TabIndex = 30;
            // 
            // btn_rectangle_random
            // 
            this.btn_rectangle_random.Location = new System.Drawing.Point(440, 90);
            this.btn_rectangle_random.Name = "btn_rectangle_random";
            this.btn_rectangle_random.Size = new System.Drawing.Size(150, 50);
            this.btn_rectangle_random.TabIndex = 29;
            this.btn_rectangle_random.Text = "Random";
            this.btn_rectangle_random.UseVisualStyleBackColor = true;
            this.btn_rectangle_random.Click += new System.EventHandler(this.btn_rectangle_random_Click);
            // 
            // btn_rectangle_send
            // 
            this.btn_rectangle_send.Location = new System.Drawing.Point(300, 330);
            this.btn_rectangle_send.Name = "btn_rectangle_send";
            this.btn_rectangle_send.Size = new System.Drawing.Size(180, 100);
            this.btn_rectangle_send.TabIndex = 28;
            this.btn_rectangle_send.Text = "Send";
            this.btn_rectangle_send.UseVisualStyleBackColor = true;
            this.btn_rectangle_send.Click += new System.EventHandler(this.btn_rectangle_send_Click);
            // 
            // tab_polygon
            // 
            this.tab_polygon.AccessibleName = "";
            this.tab_polygon.Controls.Add(this.btn_polygon_add_point);
            this.tab_polygon.Controls.Add(this.lb_polygon_color);
            this.tab_polygon.Controls.Add(this.lb_polygon_y1);
            this.tab_polygon.Controls.Add(this.lb_polygon_x1);
            this.tab_polygon.Controls.Add(this.tb_polygon_color);
            this.tab_polygon.Controls.Add(this.tb_polygon_y1);
            this.tab_polygon.Controls.Add(this.tb_polygon_x1);
            this.tab_polygon.Controls.Add(this.btn_polygon_random);
            this.tab_polygon.Controls.Add(this.btn_polygon_send);
            this.tab_polygon.Controls.Add(this.list_polygon);
            this.tab_polygon.Location = new System.Drawing.Point(4, 22);
            this.tab_polygon.Name = "tab_polygon";
            this.tab_polygon.Padding = new System.Windows.Forms.Padding(3);
            this.tab_polygon.Size = new System.Drawing.Size(796, 511);
            this.tab_polygon.TabIndex = 4;
            this.tab_polygon.Text = "Polygon";
            this.tab_polygon.UseVisualStyleBackColor = true;
            // 
            // btn_polygon_add_point
            // 
            this.btn_polygon_add_point.Location = new System.Drawing.Point(520, 90);
            this.btn_polygon_add_point.Name = "btn_polygon_add_point";
            this.btn_polygon_add_point.Size = new System.Drawing.Size(75, 23);
            this.btn_polygon_add_point.TabIndex = 24;
            this.btn_polygon_add_point.Text = "Add point";
            this.btn_polygon_add_point.UseVisualStyleBackColor = true;
            this.btn_polygon_add_point.Click += new System.EventHandler(this.btn_polygon_add_point_Click);
            // 
            // lb_polygon_color
            // 
            this.lb_polygon_color.AutoSize = true;
            this.lb_polygon_color.Location = new System.Drawing.Point(460, 145);
            this.lb_polygon_color.Name = "lb_polygon_color";
            this.lb_polygon_color.Size = new System.Drawing.Size(37, 13);
            this.lb_polygon_color.TabIndex = 23;
            this.lb_polygon_color.Text = "Color: ";
            // 
            // lb_polygon_y1
            // 
            this.lb_polygon_y1.AutoSize = true;
            this.lb_polygon_y1.Location = new System.Drawing.Point(460, 60);
            this.lb_polygon_y1.Name = "lb_polygon_y1";
            this.lb_polygon_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_polygon_y1.TabIndex = 20;
            this.lb_polygon_y1.Text = "Point y1:";
            // 
            // lb_polygon_x1
            // 
            this.lb_polygon_x1.AutoSize = true;
            this.lb_polygon_x1.Location = new System.Drawing.Point(460, 30);
            this.lb_polygon_x1.Name = "lb_polygon_x1";
            this.lb_polygon_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_polygon_x1.TabIndex = 19;
            this.lb_polygon_x1.Text = "Point x1: ";
            // 
            // tb_polygon_color
            // 
            this.tb_polygon_color.Location = new System.Drawing.Point(520, 140);
            this.tb_polygon_color.Name = "tb_polygon_color";
            this.tb_polygon_color.Size = new System.Drawing.Size(150, 20);
            this.tb_polygon_color.TabIndex = 18;
            // 
            // tb_polygon_y1
            // 
            this.tb_polygon_y1.Location = new System.Drawing.Point(520, 55);
            this.tb_polygon_y1.Name = "tb_polygon_y1";
            this.tb_polygon_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_polygon_y1.TabIndex = 15;
            // 
            // tb_polygon_x1
            // 
            this.tb_polygon_x1.Location = new System.Drawing.Point(520, 25);
            this.tb_polygon_x1.Name = "tb_polygon_x1";
            this.tb_polygon_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_polygon_x1.TabIndex = 14;
            // 
            // btn_polygon_random
            // 
            this.btn_polygon_random.Location = new System.Drawing.Point(520, 205);
            this.btn_polygon_random.Name = "btn_polygon_random";
            this.btn_polygon_random.Size = new System.Drawing.Size(150, 50);
            this.btn_polygon_random.TabIndex = 13;
            this.btn_polygon_random.Text = "Random";
            this.btn_polygon_random.UseVisualStyleBackColor = true;
            this.btn_polygon_random.Click += new System.EventHandler(this.btn_polygon_random_Click);
            // 
            // btn_polygon_send
            // 
            this.btn_polygon_send.Location = new System.Drawing.Point(300, 330);
            this.btn_polygon_send.Name = "btn_polygon_send";
            this.btn_polygon_send.Size = new System.Drawing.Size(180, 100);
            this.btn_polygon_send.TabIndex = 12;
            this.btn_polygon_send.Text = "Send";
            this.btn_polygon_send.UseVisualStyleBackColor = true;
            this.btn_polygon_send.Click += new System.EventHandler(this.btn_polygon_send_Click);
            // 
            // list_polygon
            // 
            this.list_polygon.FormattingEnabled = true;
            this.list_polygon.Location = new System.Drawing.Point(20, 30);
            this.list_polygon.Name = "list_polygon";
            this.list_polygon.Size = new System.Drawing.Size(300, 121);
            this.list_polygon.TabIndex = 0;
            // 
            // tab_circle
            // 
            this.tab_circle.Controls.Add(this.lb_circle_color);
            this.tab_circle.Controls.Add(this.lb_circle_radius);
            this.tab_circle.Controls.Add(this.lb_circle_y1);
            this.tab_circle.Controls.Add(this.lb_circle_x1);
            this.tab_circle.Controls.Add(this.tb_circle_color);
            this.tab_circle.Controls.Add(this.tb_circle_radius);
            this.tab_circle.Controls.Add(this.tb_circle_y1);
            this.tab_circle.Controls.Add(this.tb_circle_x1);
            this.tab_circle.Controls.Add(this.btn_circle_random);
            this.tab_circle.Controls.Add(this.btn_circle_send);
            this.tab_circle.Location = new System.Drawing.Point(4, 22);
            this.tab_circle.Name = "tab_circle";
            this.tab_circle.Padding = new System.Windows.Forms.Padding(3);
            this.tab_circle.Size = new System.Drawing.Size(796, 511);
            this.tab_circle.TabIndex = 5;
            this.tab_circle.Text = "Circle";
            this.tab_circle.UseVisualStyleBackColor = true;
            // 
            // lb_circle_color
            // 
            this.lb_circle_color.AutoSize = true;
            this.lb_circle_color.Location = new System.Drawing.Point(400, 30);
            this.lb_circle_color.Name = "lb_circle_color";
            this.lb_circle_color.Size = new System.Drawing.Size(37, 13);
            this.lb_circle_color.TabIndex = 51;
            this.lb_circle_color.Text = "Color: ";
            // 
            // lb_circle_radius
            // 
            this.lb_circle_radius.AutoSize = true;
            this.lb_circle_radius.Location = new System.Drawing.Point(20, 90);
            this.lb_circle_radius.Name = "lb_circle_radius";
            this.lb_circle_radius.Size = new System.Drawing.Size(43, 13);
            this.lb_circle_radius.TabIndex = 50;
            this.lb_circle_radius.Text = "Radius:";
            // 
            // lb_circle_y1
            // 
            this.lb_circle_y1.AutoSize = true;
            this.lb_circle_y1.Location = new System.Drawing.Point(20, 60);
            this.lb_circle_y1.Name = "lb_circle_y1";
            this.lb_circle_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_circle_y1.TabIndex = 48;
            this.lb_circle_y1.Text = "Point y1:";
            // 
            // lb_circle_x1
            // 
            this.lb_circle_x1.AutoSize = true;
            this.lb_circle_x1.Location = new System.Drawing.Point(20, 30);
            this.lb_circle_x1.Name = "lb_circle_x1";
            this.lb_circle_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_circle_x1.TabIndex = 47;
            this.lb_circle_x1.Text = "Point x1: ";
            // 
            // tb_circle_color
            // 
            this.tb_circle_color.Location = new System.Drawing.Point(440, 25);
            this.tb_circle_color.Name = "tb_circle_color";
            this.tb_circle_color.Size = new System.Drawing.Size(150, 20);
            this.tb_circle_color.TabIndex = 46;
            // 
            // tb_circle_radius
            // 
            this.tb_circle_radius.Location = new System.Drawing.Point(75, 85);
            this.tb_circle_radius.Name = "tb_circle_radius";
            this.tb_circle_radius.Size = new System.Drawing.Size(190, 20);
            this.tb_circle_radius.TabIndex = 44;
            // 
            // tb_circle_y1
            // 
            this.tb_circle_y1.Location = new System.Drawing.Point(75, 55);
            this.tb_circle_y1.Name = "tb_circle_y1";
            this.tb_circle_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_circle_y1.TabIndex = 43;
            // 
            // tb_circle_x1
            // 
            this.tb_circle_x1.Location = new System.Drawing.Point(75, 25);
            this.tb_circle_x1.Name = "tb_circle_x1";
            this.tb_circle_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_circle_x1.TabIndex = 42;
            // 
            // btn_circle_random
            // 
            this.btn_circle_random.Location = new System.Drawing.Point(440, 90);
            this.btn_circle_random.Name = "btn_circle_random";
            this.btn_circle_random.Size = new System.Drawing.Size(150, 50);
            this.btn_circle_random.TabIndex = 41;
            this.btn_circle_random.Text = "Random";
            this.btn_circle_random.UseVisualStyleBackColor = true;
            this.btn_circle_random.Click += new System.EventHandler(this.btn_circle_random_Click);
            // 
            // btn_circle_send
            // 
            this.btn_circle_send.Location = new System.Drawing.Point(300, 330);
            this.btn_circle_send.Name = "btn_circle_send";
            this.btn_circle_send.Size = new System.Drawing.Size(180, 100);
            this.btn_circle_send.TabIndex = 40;
            this.btn_circle_send.Text = "Send";
            this.btn_circle_send.UseVisualStyleBackColor = true;
            this.btn_circle_send.Click += new System.EventHandler(this.btn_circle_send_Click);
            // 
            // tab_ellipse
            // 
            this.tab_ellipse.Controls.Add(this.lb_ellipse_ry);
            this.tab_ellipse.Controls.Add(this.tb_ellipse_ry);
            this.tab_ellipse.Controls.Add(this.lb_ellipse_color);
            this.tab_ellipse.Controls.Add(this.lb_ellipse_rx);
            this.tab_ellipse.Controls.Add(this.lb_ellipse_y1);
            this.tab_ellipse.Controls.Add(this.lb_ellipse_x1);
            this.tab_ellipse.Controls.Add(this.tb_ellipse_color);
            this.tab_ellipse.Controls.Add(this.tb_ellipse_rx);
            this.tab_ellipse.Controls.Add(this.tb_ellipse_y1);
            this.tab_ellipse.Controls.Add(this.tb_ellipse_x1);
            this.tab_ellipse.Controls.Add(this.btn_ellipse_random);
            this.tab_ellipse.Controls.Add(this.btn_ellipse_send);
            this.tab_ellipse.Location = new System.Drawing.Point(4, 22);
            this.tab_ellipse.Name = "tab_ellipse";
            this.tab_ellipse.Padding = new System.Windows.Forms.Padding(3);
            this.tab_ellipse.Size = new System.Drawing.Size(796, 511);
            this.tab_ellipse.TabIndex = 6;
            this.tab_ellipse.Text = "Ellipse";
            this.tab_ellipse.UseVisualStyleBackColor = true;
            // 
            // lb_ellipse_ry
            // 
            this.lb_ellipse_ry.AutoSize = true;
            this.lb_ellipse_ry.Location = new System.Drawing.Point(20, 120);
            this.lb_ellipse_ry.Name = "lb_ellipse_ry";
            this.lb_ellipse_ry.Size = new System.Drawing.Size(51, 13);
            this.lb_ellipse_ry.TabIndex = 63;
            this.lb_ellipse_ry.Text = "Radius y:";
            // 
            // tb_ellipse_ry
            // 
            this.tb_ellipse_ry.Location = new System.Drawing.Point(75, 115);
            this.tb_ellipse_ry.Name = "tb_ellipse_ry";
            this.tb_ellipse_ry.Size = new System.Drawing.Size(190, 20);
            this.tb_ellipse_ry.TabIndex = 62;
            // 
            // lb_ellipse_color
            // 
            this.lb_ellipse_color.AutoSize = true;
            this.lb_ellipse_color.Location = new System.Drawing.Point(400, 30);
            this.lb_ellipse_color.Name = "lb_ellipse_color";
            this.lb_ellipse_color.Size = new System.Drawing.Size(37, 13);
            this.lb_ellipse_color.TabIndex = 61;
            this.lb_ellipse_color.Text = "Color: ";
            // 
            // lb_ellipse_rx
            // 
            this.lb_ellipse_rx.AutoSize = true;
            this.lb_ellipse_rx.Location = new System.Drawing.Point(20, 90);
            this.lb_ellipse_rx.Name = "lb_ellipse_rx";
            this.lb_ellipse_rx.Size = new System.Drawing.Size(51, 13);
            this.lb_ellipse_rx.TabIndex = 60;
            this.lb_ellipse_rx.Text = "Radius x:";
            // 
            // lb_ellipse_y1
            // 
            this.lb_ellipse_y1.AutoSize = true;
            this.lb_ellipse_y1.Location = new System.Drawing.Point(20, 60);
            this.lb_ellipse_y1.Name = "lb_ellipse_y1";
            this.lb_ellipse_y1.Size = new System.Drawing.Size(48, 13);
            this.lb_ellipse_y1.TabIndex = 59;
            this.lb_ellipse_y1.Text = "Point y1:";
            // 
            // lb_ellipse_x1
            // 
            this.lb_ellipse_x1.AutoSize = true;
            this.lb_ellipse_x1.Location = new System.Drawing.Point(20, 30);
            this.lb_ellipse_x1.Name = "lb_ellipse_x1";
            this.lb_ellipse_x1.Size = new System.Drawing.Size(51, 13);
            this.lb_ellipse_x1.TabIndex = 58;
            this.lb_ellipse_x1.Text = "Point x1: ";
            // 
            // tb_ellipse_color
            // 
            this.tb_ellipse_color.Location = new System.Drawing.Point(440, 25);
            this.tb_ellipse_color.Name = "tb_ellipse_color";
            this.tb_ellipse_color.Size = new System.Drawing.Size(150, 20);
            this.tb_ellipse_color.TabIndex = 57;
            // 
            // tb_ellipse_rx
            // 
            this.tb_ellipse_rx.Location = new System.Drawing.Point(75, 85);
            this.tb_ellipse_rx.Name = "tb_ellipse_rx";
            this.tb_ellipse_rx.Size = new System.Drawing.Size(190, 20);
            this.tb_ellipse_rx.TabIndex = 56;
            // 
            // tb_ellipse_y1
            // 
            this.tb_ellipse_y1.Location = new System.Drawing.Point(75, 55);
            this.tb_ellipse_y1.Name = "tb_ellipse_y1";
            this.tb_ellipse_y1.Size = new System.Drawing.Size(190, 20);
            this.tb_ellipse_y1.TabIndex = 55;
            // 
            // tb_ellipse_x1
            // 
            this.tb_ellipse_x1.Location = new System.Drawing.Point(75, 25);
            this.tb_ellipse_x1.Name = "tb_ellipse_x1";
            this.tb_ellipse_x1.Size = new System.Drawing.Size(190, 20);
            this.tb_ellipse_x1.TabIndex = 54;
            // 
            // btn_ellipse_random
            // 
            this.btn_ellipse_random.Location = new System.Drawing.Point(440, 90);
            this.btn_ellipse_random.Name = "btn_ellipse_random";
            this.btn_ellipse_random.Size = new System.Drawing.Size(150, 50);
            this.btn_ellipse_random.TabIndex = 53;
            this.btn_ellipse_random.Text = "Random";
            this.btn_ellipse_random.UseVisualStyleBackColor = true;
            this.btn_ellipse_random.Click += new System.EventHandler(this.btn_ellipse_random_Click);
            // 
            // btn_ellipse_send
            // 
            this.btn_ellipse_send.Location = new System.Drawing.Point(300, 330);
            this.btn_ellipse_send.Name = "btn_ellipse_send";
            this.btn_ellipse_send.Size = new System.Drawing.Size(180, 100);
            this.btn_ellipse_send.TabIndex = 52;
            this.btn_ellipse_send.Text = "Send";
            this.btn_ellipse_send.UseVisualStyleBackColor = true;
            this.btn_ellipse_send.Click += new System.EventHandler(this.btn_ellipse_send_Click);
            // 
            // lb_connection
            // 
            this.lb_connection.AutoSize = true;
            this.lb_connection.Location = new System.Drawing.Point(852, 123);
            this.lb_connection.Name = "lb_connection";
            this.lb_connection.Size = new System.Drawing.Size(98, 13);
            this.lb_connection.TabIndex = 1;
            this.lb_connection.Text = "Connection status: ";
            // 
            // panel_connection
            // 
            this.panel_connection.BackColor = System.Drawing.Color.Red;
            this.panel_connection.Location = new System.Drawing.Point(822, 139);
            this.panel_connection.Name = "panel_connection";
            this.panel_connection.Size = new System.Drawing.Size(150, 150);
            this.panel_connection.TabIndex = 2;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.panel_connection);
            this.Controls.Add(this.lb_connection);
            this.Controls.Add(this.tabs);
            this.Name = "Form1";
            this.Text = "LV6 client";
            this.tabs.ResumeLayout(false);
            this.tab_connect.ResumeLayout(false);
            this.tab_connect.PerformLayout();
            this.tab_line.ResumeLayout(false);
            this.tab_line.PerformLayout();
            this.tab_triangle.ResumeLayout(false);
            this.tab_triangle.PerformLayout();
            this.tab_rectangle.ResumeLayout(false);
            this.tab_rectangle.PerformLayout();
            this.tab_polygon.ResumeLayout(false);
            this.tab_polygon.PerformLayout();
            this.tab_circle.ResumeLayout(false);
            this.tab_circle.PerformLayout();
            this.tab_ellipse.ResumeLayout(false);
            this.tab_ellipse.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tab_connect;
        private System.Windows.Forms.TabPage tab_line;
        private System.Windows.Forms.TabPage tab_triangle;
        private System.Windows.Forms.TabPage tab_rectangle;
        private System.Windows.Forms.TabPage tab_polygon;
        private System.Windows.Forms.TabPage tab_circle;
        private System.Windows.Forms.TabPage tab_ellipse;
        private System.Windows.Forms.Label lb_connection;
        private System.Windows.Forms.Panel panel_connection;
        private System.Windows.Forms.Label lb_port;
        private System.Windows.Forms.Label lb_host;
        private System.Windows.Forms.TextBox tb_port;
        private System.Windows.Forms.TextBox tb_host;
        private System.Windows.Forms.Button btn_close_connection;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Label lb_line_color;
        private System.Windows.Forms.Label lb_line_x2;
        private System.Windows.Forms.Label lb_line_y2;
        private System.Windows.Forms.Label lb_line_y1;
        private System.Windows.Forms.Label lb_line_x1;
        private System.Windows.Forms.TextBox tb_line_color;
        private System.Windows.Forms.TextBox tb_line_y2;
        private System.Windows.Forms.TextBox tb_line_x2;
        private System.Windows.Forms.TextBox tb_line_y1;
        private System.Windows.Forms.TextBox tb_line_x1;
        private System.Windows.Forms.Button btn_line_random;
        private System.Windows.Forms.Button btn_line_send;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lb_trangle_x3;
        private System.Windows.Forms.Label lb_trangle_y3;
        private System.Windows.Forms.TextBox tb_triangle_y3;
        private System.Windows.Forms.TextBox tb_triangle_x3;
        private System.Windows.Forms.Label lb_triangle_color;
        private System.Windows.Forms.Label lb_trangle_x2;
        private System.Windows.Forms.Label lb_trangle_y2;
        private System.Windows.Forms.Label lb_triangle_y1;
        private System.Windows.Forms.Label lb_triangle_x1;
        private System.Windows.Forms.TextBox tb_triangle_color;
        private System.Windows.Forms.TextBox tb_triangle_y2;
        private System.Windows.Forms.TextBox tb_triangle_x2;
        private System.Windows.Forms.TextBox tb_triangle_y1;
        private System.Windows.Forms.TextBox tb_triangle_x1;
        private System.Windows.Forms.Button btn_triangle_random;
        private System.Windows.Forms.Button btn_triangle_send;
        private System.Windows.Forms.Label lb_rectangle_color;
        private System.Windows.Forms.Label lb_rectangle_height;
        private System.Windows.Forms.Label lb_rectangle_width;
        private System.Windows.Forms.Label lb_rectangle_y1;
        private System.Windows.Forms.Label lb_rectangle_x1;
        private System.Windows.Forms.TextBox tb_rectangle_color;
        private System.Windows.Forms.TextBox tb_rectangle_width;
        private System.Windows.Forms.TextBox tb_rectangle_height;
        private System.Windows.Forms.TextBox tb_rectangle_y1;
        private System.Windows.Forms.TextBox tb_rectangle_x1;
        private System.Windows.Forms.Button btn_rectangle_random;
        private System.Windows.Forms.Button btn_rectangle_send;
        private System.Windows.Forms.Label lb_polygon_color;
        private System.Windows.Forms.Label lb_polygon_y1;
        private System.Windows.Forms.Label lb_polygon_x1;
        private System.Windows.Forms.TextBox tb_polygon_color;
        private System.Windows.Forms.TextBox tb_polygon_y1;
        private System.Windows.Forms.TextBox tb_polygon_x1;
        private System.Windows.Forms.Button btn_polygon_random;
        private System.Windows.Forms.Button btn_polygon_send;
        private System.Windows.Forms.ListBox list_polygon;
        private System.Windows.Forms.Button btn_polygon_add_point;
        private System.Windows.Forms.Label lb_circle_color;
        private System.Windows.Forms.Label lb_circle_radius;
        private System.Windows.Forms.Label lb_circle_y1;
        private System.Windows.Forms.Label lb_circle_x1;
        private System.Windows.Forms.TextBox tb_circle_color;
        private System.Windows.Forms.TextBox tb_circle_radius;
        private System.Windows.Forms.TextBox tb_circle_y1;
        private System.Windows.Forms.TextBox tb_circle_x1;
        private System.Windows.Forms.Button btn_circle_random;
        private System.Windows.Forms.Button btn_circle_send;
        private System.Windows.Forms.Label lb_ellipse_color;
        private System.Windows.Forms.Label lb_ellipse_rx;
        private System.Windows.Forms.Label lb_ellipse_y1;
        private System.Windows.Forms.Label lb_ellipse_x1;
        private System.Windows.Forms.TextBox tb_ellipse_color;
        private System.Windows.Forms.TextBox tb_ellipse_rx;
        private System.Windows.Forms.TextBox tb_ellipse_y1;
        private System.Windows.Forms.TextBox tb_ellipse_x1;
        private System.Windows.Forms.Button btn_ellipse_random;
        private System.Windows.Forms.Button btn_ellipse_send;
        private System.Windows.Forms.Label lb_ellipse_ry;
        private System.Windows.Forms.TextBox tb_ellipse_ry;
    }
}

