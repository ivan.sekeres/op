import tkinter as tk 
from tkinter import filedialog, messagebox

class Graf_lik():
    color = ""
    point1 = (0, 0)

    def __init__(self, color, point1):
        self.point1 = point1
        self.color = color
    
    def set_color(self, color):
        self.color = color

    def get_color(self):
        return self.color
    
    def draw(self):
        pass

class Line(Graf_lik):
    point2 = (0, 0)

    def __init__(self, color, point1, point2):
        super().__init__(color, point1)
        self.point2 = point2

    def draw(self, canvas):
        canvas.create_line(self.point1[0], self.point1[1], 
                           self.point2[0], self.point2[1], 
                           fill=self.color)

class Triangle(Line):
    point3 = (0, 0)

    def __init__(self, color, point1, point2, point3):
        super().__init__(color, point1, point2)
        self.point3 = point3

    def draw(self, canvas):
        canvas.create_polygon([self.point1[0], self.point1[1], 
                               self.point2[0], self.point2[1], 
                               self.point3[0], self.point3[1]], 
                               outline=self.color, fill='')

class Rectangle(Graf_lik):
    rec_height = 0.0
    rec_width = 0.0
    
    def __init__(self, color, point1, height, width):
        super().__init__(color, point1)
        self.rec_width = width
        self.rec_height = height

    def draw(self, canvas):
        canvas.create_rectangle(self.point1[0], self.point1[1], 
                                float(self.rec_width) + float(self.point1[0]),
                                float(self.rec_height) + float(self.point1[1]), 
                                outline=self.color, fill='')

class Polygon(Graf_lik):
    points = [] 

    def __init__(self, color, point1, points):
        super().__init__(color, point1)
        self.points.clear()
        self.points.append(point1)
        for point in points:
            self.points.append(point)
    
    def draw(self, canvas):
        canvas.create_polygon(self.points, outline=self.color, fill='')

class Circle(Graf_lik):
    radius_x = 0

    def __init__(self, color, point1, radius_x):
        super().__init__(color, point1)
        self.radius_x = radius_x
    
    def draw(self, canvas):
        canvas.create_oval(float(self.point1[0])-float(self.radius_x), 
                           float(self.point1[1])-float(self.radius_x), 
                           float(self.point1[0])+float(self.radius_x), 
                           float(self.point1[1])+float(self.radius_x), 
                           outline=self.color, fill='')
        
class Ellipse(Circle):
    radius_y = 0

    def __init__(self, color, point1, radius_x, radius_y):
        super().__init__(color, point1, radius_x)
        self.radius_y = radius_y

    def draw(self, canvas):
        canvas.create_oval(float(self.point1[0])-float(self.radius_x), 
                           float(self.point1[1])-float(self.radius_y), 
                           float(self.point1[0])+float(self.radius_x), 
                           float(self.point1[1])+float(self.radius_y), 
                           outline=self.color, fill='')

class Application(tk.Frame):
    def __init__(self, master = None):
        tk.Frame.__init__(self, master)
        self.master = master
        self.pack()
        self.create_canvas()
        self.create_menu()

    def create_canvas(self):
        self.master.title("LV5")
        self.canvas = tk.Canvas(root, width=800, height=600, bg="#999999")
        self.canvas.pack()

    def create_menu(self):
        self.menu_bar = tk.Menu(self.master)
        self.master.config(menu=self.menu_bar)
        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="File", menu=self.file_menu)
        self.file_menu.add_command(label="Open", command=self.open_file)
        self.file_menu.add_command(label="Exit", command=self.exit)
        self.pack()

    def exit(self):
        if messagebox.askokcancel("Exit", "Do you want to quit?"):
            self.quit()

    def open_file(self):
        file_path = filedialog.askopenfilename(filetypes=[("Lik Files", "*.*")])
        if file_path:
            with open(file_path, 'r') as file:
                for line in file: 
                    line = line.strip()
                    values = line.split()

                    if values[0] == "Line":
                        Line(values[1], (values[2], values[3]), (values[4], values[5])).draw(self.canvas)
                    elif values[0] == "Triangle":
                        Triangle(values[1], (values[2], values[3]), (values[4], values[5]), (values[6], values[7])).draw(self.canvas)
                    elif values[0] == "Rectangle":
                        Rectangle(values[1], (values[2], values[3]), values[4], values[5]).draw(self.canvas)
                    elif values[0] == "Polygon":
                        points = []
                        points.clear()
                        for i in range(4, int(len(values)), 2):
                            temp_point = (values[i], values[i+1])
                            points.append(temp_point)
                        Polygon(values[1], (values[2], values[3]), points).draw(self.canvas)
                    elif values[0] == "Circle":
                        Circle(values[1], (values[2], values[3]), values[4]).draw(self.canvas)
                    elif values[0] == "Ellipse":
                        Ellipse(values[1], (values[2], values[3]), values[4], values[5]).draw(self.canvas)
                    else:
                        pass
                        
if __name__ == "__main__":
    root = tk.Tk()
    app = Application(root)
    app.mainloop()