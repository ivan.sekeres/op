﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zadatak2
{
    public partial class Form1 : Form
    {
        private double result1;
        private double result2;
        private double a = 0;
        private double b = 0;
        private double c = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(!double.TryParse(textBox1.Text, out a))
            { 
                a = 0;
            }
            calculate_root(a, b, c);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox2.Text, out b))
            {
                b = 0;
            }
            calculate_root(a, b, c);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox3.Text, out c))
            {
                c = 0;
            }
            calculate_root(a, b, c);
        }

        private void calculate_root(double a, double b, double c)
        {
            double root;
            root =  b * b - (4 * a * c);

            if(root < 0)
            {
                root *= (-1);
                root = Math.Sqrt(root);
                root /= (2 * a);
                result1 = ((-1) * b) / (2 * a);
                result2 = ((-1) * b) / (2 * a);
                label5.Text = result1.ToString() + "+" + root.ToString() + "i";
                label7.Text = result2.ToString() + "-" + root.ToString() + "i";
            }
            else
            {
                result1 = ((-1) * b + Math.Sqrt(root)) / 2 * a;
                result2 = ((-1) * b - Math.Sqrt(root)) / 2 * a;
                label5.Text = result1.ToString();
                label7.Text = result2.ToString();
            } 
        }
    }
}
