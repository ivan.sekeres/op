﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zadatak1
{
    public partial class Form1 : Form
    {
        private int counter1 = 0;
        private int counter2 = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            counter1++;
            label1.Text = "Tipka 1 pritisnuta " + counter1 + " puta";
            UpdateLabel3("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            counter2++;
            label2.Text = "Tipka 2 pritisnuta " + counter2 + " puta";
            UpdateLabel3("2");
        }

        private void UpdateLabel3(string text)
        {
            label3.Text = "Pritisnuta tipka " + text;
        }
    }
}