﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Rectangle : Line
    {
        public Rectangle(GrafObj grafobj, Point point) : base(grafobj, point)
        {
            this.endPoint = point;
        }

        public override void DrawGrafObj(Graphics graphics)
        {
            graphics.DrawRectangle(new Pen(GetColor()),
                            Math.Min(Coordinate.X, endPoint.X),
                            Math.Min(Coordinate.Y, endPoint.Y),
                            Math.Abs(endPoint.X - Coordinate.X),
                            Math.Abs(endPoint.Y - Coordinate.Y));
        }

    }
}
