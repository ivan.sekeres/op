﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LV2
{
    public partial class Form1 : Form
    {
        Point mStartPoint, mCurrPoint;
        Point ptPolygon;
        bool bMouseDown;
        Graphics mGraphicsHelper;
        Color color;

        private const int PEN_WIDTH = 1;

        GrafObj grafObj, grafPol;
        int counter = 0;

        Polygon polygon;
        GrafObj gObject;
        List<GrafObj> ObjectList = new List<GrafObj>();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            this.mStartPoint = e.Location;
            this.bMouseDown = true;
            this.mCurrPoint = Point.Empty;

            if (rbBlack.Checked)
            {
                color = Color.Black;
            }
            else if (rbRed.Checked)
            {
                color = Color.Red;
            }
            else if (rbBlue.Checked)
            {
                color = Color.Blue;
            }

            grafObj = new GrafObj(color, this.mStartPoint);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if(this.bMouseDown)
            {
                this.mCurrPoint = e.Location;
                this.Invalidate();
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if(!rbPolygon.Checked)
            {
                this.bMouseDown = false;
                ObjectList.Add(gObject);
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            foreach (GrafObj item in ObjectList)
            {
                if(item != null)
                    item.DrawGrafObj(mGraphicsHelper);
            }

            if (this.bMouseDown)
            {
                if (rbLine.Checked)
                {
                    gObject = new Line(grafObj, this.mCurrPoint);
                    gObject.DrawGrafObj(mGraphicsHelper);
                }
                else if (rbRectangle.Checked)
                {
                    gObject = new Rectangle(grafObj, this.mCurrPoint);
                    gObject.DrawGrafObj(mGraphicsHelper);
                }
                else if (rbCircle.Checked)
                {
                    gObject = new Circle(grafObj, this.mCurrPoint);
                    gObject.DrawGrafObj(mGraphicsHelper);
                }
                else if (rbEllipse.Checked)
                {
                    gObject = new Ellipse(grafObj, this.mCurrPoint);
                    gObject.DrawGrafObj(mGraphicsHelper);
                }
            }
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            ptPolygon = e.Location;
            grafPol = new GrafObj(color, ptPolygon);
            
            if (rbPolygon.Checked && counter == 0)
            {
                polygon = new Polygon(grafPol);
                counter++;
            }
            else if (rbPolygon.Checked && counter <= 2)
            {
                polygon.AddPoint(grafPol);
                counter++;
            }
            else if (rbPolygon.Checked && counter <= 3)
            {
                polygon.AddPoint(grafPol);
                polygon.DrawGrafObj(mGraphicsHelper);
                ObjectList.Add(polygon);
                counter = 0;
            }

            grafPol = null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bMouseDown = false;
            this.mCurrPoint = Point.Empty;
            mGraphicsHelper = this.CreateGraphics();
        }
    }
}
