﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Ellipse : Circle
    {
        public float height;

        public Ellipse(GrafObj grafobj, Point point) : base(grafobj, point)
        {
            this.endPoint = point;
            radius = (float)Math.Sqrt(Math.Pow(endPoint.X - Coordinate.X, 2) + Math.Pow(endPoint.Y - Coordinate.Y, 2));
            height = Math.Abs(Coordinate.Y - endPoint.Y);
        }

        public override void DrawGrafObj(Graphics graphics)
        {
            graphics.DrawEllipse(new Pen(this.GetColor()), Math.Min(this.Coordinate.X, endPoint.X),
                            Math.Min(Coordinate.Y, endPoint.Y), radius, height);
        }

    }
}
