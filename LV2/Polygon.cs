﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Polygon : GrafObj
    {

        public List<Point> points = new List<Point>();

        public void AddPoint(GrafObj grafobj)
        {
            points.Add(grafobj.Coordinate);
        }

        public Polygon(GrafObj grafobj) : base(grafobj)
        {
            this.AddPoint(grafobj);
        }

        public override void DrawGrafObj(Graphics graphics)
        {
            Point[] pointsArray = points.ToArray();
            graphics.DrawPolygon(new Pen(this.GetColor()), pointsArray);
        }
    }
}
