﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Line : GrafObj
    {
        public Point endPoint;

        public Line(GrafObj grafobj, Point point) : base(grafobj)
        {
            this.endPoint = point;
        }
        public override void DrawGrafObj(Graphics graphics)
        {
            graphics.DrawLine(new Pen(this.GetColor()), Coordinate, endPoint);
        }
    }
}
