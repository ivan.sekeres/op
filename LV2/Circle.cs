﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Circle : GrafObj
    {
        public Point endPoint;
        public float radius;

        public Circle(GrafObj grafobj, Point point) : base(grafobj)
        {
            this.endPoint = point;
            radius = (float)Math.Sqrt(Math.Pow(endPoint.X - Coordinate.X, 2) + Math.Pow(endPoint.Y - Coordinate.Y, 2));
        }
        public override void DrawGrafObj(Graphics graphics)
        {

            graphics.DrawEllipse(new Pen(GetColor()), Math.Min(Coordinate.X, endPoint.X),
                            Math.Min(Coordinate.Y, endPoint.Y), radius, radius);
        }
    }
}
