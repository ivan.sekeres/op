﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class GrafObj
    {
        private Color color;
        private Point point;

        public GrafObj(Color color, Point point)
        {
            this.color = color;
            this.point = point;
        }

        public GrafObj(GrafObj grafobj)
        {
            this.color = grafobj.color;
            this.point = grafobj.point;
        }

        public void SetColor(Color color)
        {
            this.color = color;
        }

        public Color GetColor()
        {
            return this.color;
        }

        public virtual void DrawGrafObj(Graphics graphics) { }

        public Point Coordinate
        {
            get { return point; }
            set { point = value; }
        }

    }
}
