﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;

namespace Example1
{
    public partial class Form1 : Form
    {

        //Define variables to be used in the app
        OleDbConnection myConnection;
        OleDbDataAdapter myOleDbDataAdapter;
        
        BindingSource bsShippers = new BindingSource();

        //Get defined connection string from App.Config (Open App.Config to see the name given to the connection string)
        private string myConnectionString = ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString; 



        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Set the DataSource of the DataGridView
            dataGridViewShippers.DataSource = bsShippers;

            //Call private method to Bind data to the DataGridView
            BindDataGrid();

            // Hide the Identity column of the table in DataGridView
            // You can comment out the following line to see what happens when the app is run
            dataGridViewShippers.Columns[0].Visible = false;
  

        }

       //Set the text values of the textboxes when the row header of the DataGridView is clicked
        private void dataGridViewShippers_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            lblID.Text = dataGridViewShippers.SelectedRows[0].Cells[0].Value.ToString();
            txtCompanyName.Text = dataGridViewShippers.SelectedRows[0].Cells[1].Value.ToString();
            txtPhone.Text = dataGridViewShippers.SelectedRows[0].Cells[2].Value.ToString();
        }

        //TO DO...
        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCompanyName.Text) && !String.IsNullOrEmpty(txtPhone.Text))
            {

                using (myConnection = new OleDbConnection(myConnectionString))
                {
                    myConnection.Open();

                    try
                    {

                        int ID = 0;
                        string findID = "SELECT MAX(ShipperID) FROM Shippers";
                        string insertString = "INSERT INTO Shippers VALUES (@ID, @CompanyName, @Phone)";

                        OleDbCommand myCom = new OleDbCommand(findID, myConnection);
                        OleDbDataReader reader = myCom.ExecuteReader();
                        while (reader.Read())
                        {
                            ID = reader.GetInt32(0);
                        }
                        reader.Close();

                        myCom.CommandText = insertString;
                        myCom.Parameters.Add("@ID", OleDbType.Integer).Value = Convert.ToInt32(ID+1);
                        myCom.Parameters.Add("@CompanyName", OleDbType.WChar).Value = txtCompanyName.Text;
                        myCom.Parameters.Add("@Phone", OleDbType.WChar).Value = txtPhone.Text;
                        myCom.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }

                    myConnection.Close();
                }

                BindDataGrid();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCompanyName.Text) && !String.IsNullOrEmpty(txtPhone.Text))
            {

                using (myConnection = new OleDbConnection(myConnectionString))
                {
                    myConnection.Open();

                    try
                    {
                        string mySqlString = "UPDATE Shippers SET CompanyName = @CompanyName, Phone = @Phone WHERE ShipperID = @ID";

                        OleDbCommand myCom = new OleDbCommand(mySqlString, myConnection);
                        myCom.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
                        myCom.Parameters.AddWithValue("@Phone", txtPhone.Text);
                        myCom.Parameters.AddWithValue("@ID", lblID.Text);  //dataGridViewShippers.SelectedRows[0].Cells[0].Value.ToString()
                        myCom.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }

                    myConnection.Close();
                }

                BindDataGrid();
            }
            
        }

        //TO DO...
        private void btnDelete_Click(object sender, EventArgs e)
        {
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();

                try
                {
                    string mySqlString = "DELETE FROM Shippers WHERE ShipperID = @ID";

                    OleDbCommand myCom = new OleDbCommand(mySqlString, myConnection);
                    myCom.Parameters.AddWithValue("@ID", lblID.Text);  
                    myCom.ExecuteNonQuery();

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                }

                myConnection.Close();
            }

            BindDataGrid();
        }

        //Method which binds the data to the DataGridView
        private void BindDataGrid()
        {

            //Set up a new Connection to the Access DB
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                //Open the connection
                myConnection.Open();

                //Create a DataAdapter
                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();

                    //Define the select command of the DataAdapter
                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SHIPPERS", myConnection);
                    //Fill the DataSet with the obtained results. The results are stored in a Table object within the DataSet named "Shippers"
                    myOleDbDataAdapter.Fill(myDataSet, "Shippers");

                    //Set the datasource of the Bindingsource to the dataset filled by the DataAdapter
                    bsShippers.DataSource = myDataSet;
                    //Since the DataSet can have more than 1 Table, specify which Table is the DataMember of the BindingSource
                    bsShippers.DataMember = "Shippers";

                    //Close connection when done
                    myConnection.Close();
                }


            }
            //Call private method to empty/clear the textboxes 
            ClearTextBoxes();

        }

        //Method which empties/clears the textboxes
        private void ClearTextBoxes()
        {
            lblID.Text = String.Empty;
            txtCompanyName.Text = String.Empty;
            txtPhone.Text = String.Empty;

        }

    }
}
