﻿namespace ConnectingToDatabaseDZ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgw_orders = new System.Windows.Forms.DataGridView();
            this.cb_employees = new System.Windows.Forms.ComboBox();
            this.cb_customers = new System.Windows.Forms.ComboBox();
            this.cb_shippers = new System.Windows.Forms.ComboBox();
            this.lb_shippers = new System.Windows.Forms.Label();
            this.lb_customers = new System.Windows.Forms.Label();
            this.lb_employees = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_orders)).BeginInit();
            this.SuspendLayout();
            // 
            // dgw_orders
            // 
            this.dgw_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_orders.Location = new System.Drawing.Point(12, 12);
            this.dgw_orders.Name = "dgw_orders";
            this.dgw_orders.Size = new System.Drawing.Size(916, 620);
            this.dgw_orders.TabIndex = 0;
            // 
            // cb_employees
            // 
            this.cb_employees.FormattingEnabled = true;
            this.cb_employees.Location = new System.Drawing.Point(937, 134);
            this.cb_employees.Name = "cb_employees";
            this.cb_employees.Size = new System.Drawing.Size(176, 21);
            this.cb_employees.TabIndex = 1;
            this.cb_employees.SelectedIndexChanged += new System.EventHandler(this.cb_employees_SelectedIndexChanged);
            // 
            // cb_customers
            // 
            this.cb_customers.FormattingEnabled = true;
            this.cb_customers.Location = new System.Drawing.Point(937, 81);
            this.cb_customers.Name = "cb_customers";
            this.cb_customers.Size = new System.Drawing.Size(176, 21);
            this.cb_customers.TabIndex = 2;
            this.cb_customers.SelectedIndexChanged += new System.EventHandler(this.cb_customers_SelectedIndexChanged);
            // 
            // cb_shippers
            // 
            this.cb_shippers.FormattingEnabled = true;
            this.cb_shippers.Location = new System.Drawing.Point(937, 28);
            this.cb_shippers.Name = "cb_shippers";
            this.cb_shippers.Size = new System.Drawing.Size(176, 21);
            this.cb_shippers.TabIndex = 3;
            this.cb_shippers.SelectedIndexChanged += new System.EventHandler(this.cb_shippers_SelectedIndexChanged);
            // 
            // lb_shippers
            // 
            this.lb_shippers.AutoSize = true;
            this.lb_shippers.Location = new System.Drawing.Point(934, 12);
            this.lb_shippers.Name = "lb_shippers";
            this.lb_shippers.Size = new System.Drawing.Size(51, 13);
            this.lb_shippers.TabIndex = 4;
            this.lb_shippers.Text = "Shippers:";
            // 
            // lb_customers
            // 
            this.lb_customers.AutoSize = true;
            this.lb_customers.Location = new System.Drawing.Point(934, 65);
            this.lb_customers.Name = "lb_customers";
            this.lb_customers.Size = new System.Drawing.Size(59, 13);
            this.lb_customers.TabIndex = 5;
            this.lb_customers.Text = "Customers:";
            // 
            // lb_employees
            // 
            this.lb_employees.AutoSize = true;
            this.lb_employees.Location = new System.Drawing.Point(934, 118);
            this.lb_employees.Name = "lb_employees";
            this.lb_employees.Size = new System.Drawing.Size(61, 13);
            this.lb_employees.TabIndex = 6;
            this.lb_employees.Text = "Employees:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 644);
            this.Controls.Add(this.lb_employees);
            this.Controls.Add(this.lb_customers);
            this.Controls.Add(this.lb_shippers);
            this.Controls.Add(this.cb_shippers);
            this.Controls.Add(this.cb_customers);
            this.Controls.Add(this.cb_employees);
            this.Controls.Add(this.dgw_orders);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_orders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgw_orders;
        private System.Windows.Forms.ComboBox cb_employees;
        private System.Windows.Forms.ComboBox cb_customers;
        private System.Windows.Forms.ComboBox cb_shippers;
        private System.Windows.Forms.Label lb_shippers;
        private System.Windows.Forms.Label lb_customers;
        private System.Windows.Forms.Label lb_employees;
    }
}

