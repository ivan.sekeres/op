﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;


namespace ConnectingToDatabaseDZ
{
    public partial class Form1 : Form
    {
        OleDbConnection myConnection;
        OleDbDataAdapter myOleDbDataAdapter;

        BindingSource bsOrders = new BindingSource();
        BindingSource bsShippers = new BindingSource();
        BindingSource bsCustomers = new BindingSource();
        BindingSource bsEmployees = new BindingSource();

        private string myConnectionString = ConfigurationManager.ConnectionStrings["ConnectingToDatabaseDZ.Properties.Settings.NorthwindConnectionString1"].ConnectionString;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();

                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM ORDERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Orders");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SHIPPERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Shippers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CUSTOMERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Customers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM EMPLOYEES", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Employees");
                    
                    DataRelation ordersShippers = new DataRelation("ShippersOrders", myDataSet.Tables["Shippers"].Columns["ShipperID"], myDataSet.Tables["Orders"].Columns["ShipVia"]);
                    myDataSet.Relations.Add(ordersShippers);

                    DataRelation ordersCustomers = new DataRelation("CustomersOrders", myDataSet.Tables["Customers"].Columns["CustomerID"], myDataSet.Tables["Orders"].Columns["CustomerID"]);
                    myDataSet.Relations.Add(ordersCustomers);

                    DataRelation ordersEmployees = new DataRelation("EmployeesOrders", myDataSet.Tables["Employees"].Columns["EmployeeID"], myDataSet.Tables["Orders"].Columns["EmployeeID"]);
                    myDataSet.Relations.Add(ordersEmployees);

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    bsShippers.DataSource = myDataSet;
                    bsShippers.DataMember = "Shippers";

                    bsCustomers.DataSource = myDataSet;
                    bsCustomers.DataMember = "Customers";

                    bsEmployees.DataSource = myDataSet;
                    bsEmployees.DataMember = "Employees";

                    dgw_orders.DataSource = bsOrders;

                    cb_shippers.DataSource = bsShippers;
                    cb_shippers.DisplayMember = "CompanyName";
                    cb_shippers.ValueMember = "ShipperID";
                    cb_shippers.SelectedIndex = 0;

                    cb_customers.DataSource = bsCustomers;
                    cb_customers.DisplayMember = "CustomerID";
                    cb_customers.ValueMember = "CustomerID";
                    cb_customers.SelectedIndex = 0;

                    cb_employees.DataSource = bsEmployees;
                    cb_employees.DisplayMember = "EmployeeID";
                    cb_employees.ValueMember = "EmployeeID";
                    cb_employees.SelectedIndex = 0;
                }
                myConnection.Close();
            }
        }

        private void cb_shippers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                bsOrders.Filter = "ShipVia = '" + cb_shippers.SelectedValue + "'" +
                                  " AND CustomerID = '" + cb_customers.SelectedValue + "'" +
                                  " AND EmployeeID = '" + cb_employees.SelectedValue + "'";
            }
            catch (Exception ex) {}
        }

        private void cb_customers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bsOrders.Filter = "ShipVia = '" + cb_shippers.SelectedValue + "'" + 
                                  " AND CustomerID = '" + cb_customers.SelectedValue + "'" + 
                                  " AND EmployeeID = '" + cb_employees.SelectedValue + "'";
            }
            catch (Exception ex) { }
        }

        private void cb_employees_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bsOrders.Filter = "ShipVia = '" + cb_shippers.SelectedValue + "'" +
                                  " AND CustomerID = '" + cb_customers.SelectedValue + "'" +
                                  " AND EmployeeID = '" + cb_employees.SelectedValue + "'";
            }
            catch (Exception ex) { }
        }
    }
}
