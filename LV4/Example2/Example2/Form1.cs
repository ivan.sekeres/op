﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;


namespace Example2
{
    public partial class Form1 : Form
    {
        OleDbConnection myConnection;
        OleDbDataAdapter myOleDbDataAdapter;
        

        BindingSource bsSuppliers = new BindingSource();
        BindingSource bsCategories = new BindingSource();
        BindingSource bsProducts = new BindingSource();

        private string myConnectionString = ConfigurationManager.ConnectionStrings["Example2.Properties.Settings.NorthwindConnectionString1"].ConnectionString; 


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Call private method to bind data to the 3 DatGridViews and 1 ComboBox
            BindData();
        }

        

        private void BindData()
        {
            using (myConnection = new OleDbConnection(myConnectionString))
            {
                myConnection.Open();

                //Test the following examples. Make sure ONLY 1 example is uncommented, ALL THE OTHER examples should be commented out!!!!
                #region Different binding examples

                /*
                //*** 1. Using Datasets ***
                
                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    DataSet dSSuppliers = new DataSet();
                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SUPPLIERS", myConnection);
                    myOleDbDataAdapter.Fill(dSSuppliers);
                    dataGridViewSuppliers.DataSource = dSSuppliers.Tables[0];


                    DataSet dSCategories = new DataSet();
                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CATEGORIES", myConnection);
                    myOleDbDataAdapter.Fill(dSCategories);
                    dataGridViewCategories.DataSource = dSCategories.Tables[0];

                    cboCategories.DataSource = dSCategories.Tables[0];
                    cboCategories.DisplayMember = "CategoryName";
                    cboCategories.ValueMember = "CategoryID";

                    DataSet dSProducts = new DataSet();
                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM Products", myConnection);
                    myOleDbDataAdapter.Fill(dSProducts);
                    dataGridViewProducts.DataSource = dSProducts.Tables[0];
                }
                */


                /*
                //*** 2. Using ONLY 1 Dataset  ***

                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();
                 
                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SUPPLIERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Suppliers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CATEGORIES", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Categories");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM Products", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Products");

                    dataGridViewSuppliers.DataSource = myDataSet;
                    dataGridViewSuppliers.DataMember = "Suppliers";

                    dataGridViewCategories.DataSource = myDataSet;
                    dataGridViewCategories.DataMember = "Categories";

                    cboCategories.DataSource = myDataSet;
                    cboCategories.DisplayMember = "Categories.CategoryName";
                    cboCategories.ValueMember = "Categories.CategoryID";

                    dataGridViewProducts.DataSource = myDataSet;
                    dataGridViewProducts.DataMember = "Products";
                }
                */

                /*
                //*** 3. Using BindingSource  ***
                
                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                    
                    DataSet myDataSet = new DataSet();

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SUPPLIERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Suppliers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CATEGORIES", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Categories");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM Products", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Products");

                    bsSuppliers.DataSource = myDataSet;
                    bsSuppliers.DataMember = "Suppliers";

                    bsCategories.DataSource = myDataSet;
                    bsCategories.DataMember = "Categories";

                    bsProducts.DataSource = myDataSet;
                    bsProducts.DataMember = "Products";

                    dataGridViewSuppliers.DataSource = bsSuppliers;
                    dataGridViewCategories.DataSource = bsCategories;
                    dataGridViewProducts.DataSource = bsProducts;


                    cboCategories.DataSource = bsCategories;
                    cboCategories.DisplayMember = "CategoryName";
                    cboCategories.ValueMember = "CategoryID";

                }
                */

                
                //*** 4. Master-child relation using BindingSource  ***
                using (myOleDbDataAdapter = new OleDbDataAdapter())
                {
                                        
                    DataSet myDataSet = new DataSet();

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM SUPPLIERS", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Suppliers");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM CATEGORIES", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Categories");

                    myOleDbDataAdapter.SelectCommand = new OleDbCommand("SELECT * FROM Products", myConnection);
                    myOleDbDataAdapter.Fill(myDataSet, "Products");

                    // Establish a relationship between the two tables.
                    DataRelation relation = new DataRelation("CategoriesProducts", myDataSet.Tables["Categories"].Columns["CategoryID"], myDataSet.Tables["Products"].Columns["CategoryID"]);
                    myDataSet.Relations.Add(relation);


                    bsSuppliers.DataSource = myDataSet;
                    bsSuppliers.DataMember = "Suppliers";

                    bsCategories.DataSource = myDataSet;
                    bsCategories.DataMember = "Categories";

                    bsProducts.DataSource = bsCategories;
                    bsProducts.DataMember = "CategoriesProducts";


                    dataGridViewSuppliers.DataSource = bsSuppliers;
                    dataGridViewCategories.DataSource = bsCategories;
                    dataGridViewProducts.DataSource = bsProducts;


                    cboCategories.DataSource = bsCategories;
                    cboCategories.DisplayMember = "CategoryName";
                    cboCategories.ValueMember = "CategoryID";

                    System.Console.WriteLine(cboCategories.ValueMember.ToString());
                }
                //*/

                #endregion



                myConnection.Close();
                
            }
            
            
                                
          


            


            


            

        }

        
        
    }
}
